var map = L.map('map', {zoomControl:false}).setView([48.51908, 9.05822], 17);
var osm_hot = L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {maxZoom: 20,attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Tiles style by <a href="https://www.hotosm.org/" target="_blank">Humanitarian OpenStreetMap Team</a> hosted by <a href="https://openstreetmap.fr/" target="_blank">OpenStreetMap France</a>'});
var OpenStreetMap_Mapnik = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {maxZoom: 19,attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'});
var Esri_WorldImagery = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'});

var markerUserAddedAED;
var queried_map_extents = []

var tagTranslation = new TagTranslation()

let activeAdding = false

osm_hot.addTo(map);
L.control.layers({'OSM HOT': osm_hot,'OSM Mapnik': OpenStreetMap_Mapnik, 'Luftbild': Esri_WorldImagery},{},{position:'topleft'}).addTo(map);

function buildOverpassApiUrl(map) {
  var bounds = map.getBounds().getSouth() + ',' + map.getBounds().getWest() + ',' + map.getBounds().getNorth() + ',' + map.getBounds().getEast();
  var query = '?data=[out:json][timeout:15];(node[emergency=defibrillator](' + bounds + '););out body geom;';
  var baseUrl = 'https://overpass.kumi.systems/api/interpreter';
  var resultUrl = baseUrl + query;
  return resultUrl;
}

function buildOsmNotesApiUrl(map) {
  var bounds = map.getBounds().getWest() + ',' + map.getBounds().getSouth() + ',' + map.getBounds().getEast() + ',' + map.getBounds().getNorth();
  var query = 'https://master.apis.dev.openstreetmap.org/api/0.6/notes.json?bbox=' + bounds;
  return query
}

function buildOsmNotesApiUploadUrl() {
  var lat = markerUserAddedAED.getLatLng().lat;
  var lon = markerUserAddedAED.getLatLng().lng;
  var form = document.getElementById('note_form')
  var text = "Hier befindet sich ein Defibrillator. \nIch schlage folgende Tags vor: \n"
    + "defibrillator:location=" + form.defibrillator_location.value + "\n"
    + "indoor=" + (form.indoor.checked ? "yes" : "no") + "\n"
    + "access=" + form.access.value + "\n\n"
    + "Kommentar: " + form.comment.value + "\n\n"
    + "#AED uploaded via defimap.de"
  url = 'https://master.apis.dev.openstreetmap.org/api/0.6/notes'
  data = {lat: lat, lon: lon, text: text} //why exactly this works ... I don't know. This IS a dict, right? How does it know my keys aren't the vars too? whatever, it works...
  return [url, data]
}

map.whenReady(updateAedsAndNotes)

map.on('moveend', function() {
  updateAedsAndNotes()
});

function updateAedsAndNotes() {
  if (map.getZoom() > 12) {
    getOverpassAndNoteData()
  document.getElementById("map-zoom-info-bar").classList.add("hidden")
  } else {  
    document.getElementById("map-zoom-info-bar").classList.remove("hidden")
  }
  //TODO auto query Notes API
}

function getOverpassAndNoteData() {
  current_bounds = map.getBounds()
  //only query APIs if the current mapextent is not contained in the bounding box of a previous query
  if (!queried_map_extents.some(bnds => bnds.contains(current_bounds))) {
    queryOverpassApi()
    queryOsmNotesApi()
  }
}

function queryOverpassApi() {

  overpassApiUrl = buildOverpassApiUrl(map)

  $.get(overpassApiUrl, function (osmDataAsJson) {
    var resultAsGeojson = osmtogeojson(osmDataAsJson);
    var resultLayer = L.geoJson(resultAsGeojson, {
      style: function (feature) {
        return { color: "#ff0000" };
      },
      onEachFeature: function (feature, layer) {
        var popupContent = "";
        popupContent = tagTranslation.formatTags(feature.properties.tags);
        popupContent += "<a class='osm-link' href='https://osm.org/" + feature.id + "' target='_blanc'>auf openstreetmap.org anzeigen</a>"
        layer.bindPopup(popupContent);
      },
      pointToLayer: function (feature, latlng) {
        return L.marker(latlng, { icon: L.icon({ iconUrl: "img/defi.svg", iconSize: [35, 35] }) });
      }
    }
    ).addTo(map);
    queried_map_extents.push(current_bounds);
  });
}

// unfortuntely you can't really use the osm note api to do stringsearch, it is
// technically still supported but not generally used or well documented for that matter
// see also ent8r.github.io/NotesReview/ 
// They also query all notes within the bounding box and THEN do the substring search
function removeNotesWithoutAED(leafletLayer) {
  leafletLayer.eachLayer(function(layer){
    if (!layer.feature.properties.comments.some(comment => stringContainsOneOrMultipleSubstrings(comment.text, ["defi", "aed"]))){
      leafletLayer.removeLayer(layer)
    }
  })
  return leafletLayer
}

function stringContainsOneOrMultipleSubstrings(string, substrings) {
  return substrings.some(subs => string.toLowerCase().includes(subs.toLowerCase()))
}

function queryOsmNotesApi() {
var notesApiUrl = buildOsmNotesApiUrl(map);
  
$.get(notesApiUrl, function (notes_result) {
    var osm_notes = L.geoJson(notes_result,{
        onEachFeature: function (feature, layer) {
            var popupContent = "";
            popupContent = popupContent + '<strong>OSM-Notiz:</strong><hr><table style="width:100%"><tr><th>OSM note id</th><td>' + feature.properties.id + '</td></tr>';
            popupContent = popupContent + '<table style="width:100%"><tr><th>Eröffnet</th><td>' + feature.properties.date_created + '</td></tr>';
            popupContent = popupContent + '</table> <hr>'
            var keys = Object.keys(feature.properties.comments);
            keys.forEach(function(key){
                popupContent = popupContent + '<br> <code> Note-Kommentar Nr. ' + (+key+1) + '</code><br> OSM User <code>' + feature.properties.comments[key].user;
                popupContent = popupContent + ' - ' + feature.properties.comments[key].date + '</code>';
                popupContent = popupContent + feature.properties.comments[key].html + '<hr>';
            });
            layer.bindPopup(popupContent, {maxHeight:"500"});
            },
            pointToLayer: function (feature, latlng) {
                var notemarker = L.icon({iconUrl:"img/note.svg", iconSize:[35,35]});
                return L.marker(latlng, {icon:notemarker});
            }
        }
)
osm_notes = removeNotesWithoutAED(osm_notes)
osm_notes.addTo(map)
});
}

function activateAdding() {
  activeAdding = true
  document.querySelector('#add-form').classList.remove('closed')
  document.querySelector('#plus').classList.add('hidden')
  document.querySelector('#chevron').classList.add('display')
  addMarkerUserAddedAED()
}

function endAdding() {
  activeAdding = false
  document.querySelector('#add-form').classList.add('closed')
  setTimeout(() => document.getElementById('plus').classList.remove('hidden'), 50)
  document.querySelector('#chevron').classList.remove('display')
  removeMarkerUserAddedAED()
}

function uploadNote() {
  ret = buildOsmNotesApiUploadUrl()
  url = ret[0]
  data = ret[1]
  console.log(url, data)
  $.post(url, data)
  endAdding()
}

document.querySelector('#addNoteFormHeader').addEventListener('click', (event) => {
  console.log(activeAdding)
  activeAdding ? endAdding() : activateAdding() 
})

document.querySelector('#upload-note-button').addEventListener('click', (event) => {
  if (window.confirm("Wirklich hochladen?")) {uploadNote();}
})

document.querySelector('#upload-cancel-button').addEventListener('click', (event) => {
  endAdding()
})

function addMarkerUserAddedAED() {
  markerUserAddedAED = L.marker(map.getCenter(),{draggable: true}).addTo(map);
}

function removeMarkerUserAddedAED() {
  if (markerUserAddedAED) {
    if (markerUserAddedAED._map) {
      map.removeLayer(markerUserAddedAED)
    }
  }
}

$("#cancel-note-button").click(function () {
  endAdding()
});