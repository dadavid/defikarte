class TagTranslation {
  constructor() {
    this.preferredLanguage = navigator.language.substr(0, 2);
    this.tagTranslations = null;
    fetch("tagtranslations.json")
      .then(response => response.json())
      .then(data => { this.tagTranslations = data; });
  }


formatTags(tags) {
  return this._formatAddressIfPresent(tags)
    + this._formatTranslatedTags(tags, ["indoor"])
    + this._formatTranslatedTags(tags, ["location", "defibrillator:location"]);
}

_formatAddressIfPresent(tags) {
  // simple first approach, does only handle most common address formats but omits addr:place for example
  let html = "";
  if (!("addr:housenumber" in tags) && "addr:street" in tags)
    html += tags["addr:street"];
  if ("addr:housenumber" in tags && "addr:street" in tags)
    html += tags["addr:street"] + " " + tags["addr:housenumber"];
  if ("addr:postcode" in tags && "addr:city" in tags) {
    if (html != "")
      html += "<br />";
    html += tags["addr:postcode"] + " " + tags["addr:city"];
  }
  if (html != "")
    html = "<div class='address'>" + html + "</div>";
  return html;
}

_formatTranslatedTags(tags, keys) {
  let html = "";
  for (let i = 0; i < keys.length; i++) {
    let key = keys[i];
    if (key in tags) {
      html += this._getTranslatedTag(key, tags[key]);
    }
  }
  if (html != "")
    html += "<br />";
  return html;
}

_getTranslatedTag(key, value) {
  let kvp = key + "=" + value;
  if (!this.preferredLanguage in this.tagTranslations) {
    console.warn("No translations for language " + this.preferredLanguage + " found");
    return;
  }
  for (let translation in this.tagTranslations[this.preferredLanguage]) {
    if (translation == kvp) {
      return this.tagTranslations[this.preferredLanguage][translation];
    }
    if (translation == key) {
      return this.tagTranslations[this.preferredLanguage][translation].replace("{{value}}", value);
    }
  }
  console.warn("no translation found for " + kvp + " in language " + this.preferredLanguage);
}
}